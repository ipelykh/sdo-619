package illya.pelykh.sdo.service.executor;

import illya.pelykh.sdo.service.callable.CallableTaskProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class CallableTaskExecutor<T> {

    private CallableTaskProvider<T> callableTaskProvider;

    public List<T> execute(List<String> urls) throws InterruptedException, IllegalStateException {
        ExecutorService executor = Executors.newWorkStealingPool();
        return executor
                .invokeAll(callableTaskProvider.calls(urls))
                .stream()
                .map(CallableTaskExecutor::futureGet)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private static <T> Optional<List<T>> futureGet(final Future<List<T>> future) {
        try {
            return Optional.of(future.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Autowired
    public void setCallableTaskProvider(CallableTaskProvider<T> callableTaskProvider) {
        this.callableTaskProvider = callableTaskProvider;
    }
}