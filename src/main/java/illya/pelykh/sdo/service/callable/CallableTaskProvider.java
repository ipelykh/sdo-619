package illya.pelykh.sdo.service.callable;

import illya.pelykh.sdo.service.api.NewsApiCallHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Service
public class CallableTaskProvider<T> {

    private NewsApiCallHandler<String, List<T>> handler;

    public List<Callable<List<T>>> calls(final List<String> urls) {

        List<Callable<List<T>>> allCallable = new ArrayList<>();

        urls.forEach(url -> allCallable.add(() -> handler.provideDto(url)));

        return allCallable;
    }

    @Autowired
    public void setHandler(NewsApiCallHandler<String, List<T>> handler) {
        this.handler = handler;
    }
}