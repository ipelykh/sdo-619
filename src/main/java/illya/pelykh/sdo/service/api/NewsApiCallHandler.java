package illya.pelykh.sdo.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import illya.pelykh.sdo.dto.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Service
public interface NewsApiCallHandler<S, T> {

    T provideDto(final S s);
}