package illya.pelykh.sdo.service.api.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import illya.pelykh.sdo.dto.ArticleDto;
import illya.pelykh.sdo.dto.ResponseDto;
import illya.pelykh.sdo.dto.impl.NewsApiResponseDto;
import illya.pelykh.sdo.service.api.NewsApiCallHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Service
public class ListArticleDtoProvider implements NewsApiCallHandler<String, List<ArticleDto>> {

    private final static Logger LOG = LogManager.getLogger(ListArticleDtoProvider.class);

    private ResponseDto dto;

    @Override
    public synchronized List<ArticleDto> provideDto(final String url) {
        LOG.info("About to retrieve response from " + url);
        RestTemplate restTemplate = new RestTemplate();

//        NewsApiResponseDto response = restTemplate.execute(url, HttpMethod.POST, requestBody, responseClass);

        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        if (HttpStatus.OK.equals(response.getStatusCode())) {

            ObjectMapper mapper = new ObjectMapper();

            try {
                dto = mapper.readValue(response.getBody(), NewsApiResponseDto.class);
                NewsApiResponseDto newsDto = (NewsApiResponseDto) dto;
                LOG.info("Successfully retrieved " + newsDto.getTotalResults() + " results from thread " + Thread.currentThread().getName());
                return newsDto.getArticles();
            } catch (IOException | HttpClientErrorException ex) {
                LOG.error(ex.getMessage());
            }
            NewsApiResponseDto responseDto = (NewsApiResponseDto) dto.getInstance(response.getStatusCode().toString());
            return responseDto.getArticles();
        }
        return null;
    }

    @Autowired
    public void setDto(ResponseDto dto) {
        this.dto = dto;
    }
}
