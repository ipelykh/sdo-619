package illya.pelykh.sdo.service.user;

import illya.pelykh.sdo.model.JwtAuthenticationResponseModel;

public interface JwtAuthenticationResponseService {

    JwtAuthenticationResponseModel findByToken(String token);

    void saveToken(JwtAuthenticationResponseModel jwtAuthenticationResponseModel);

    void deleteToken(String accessToken);
}
