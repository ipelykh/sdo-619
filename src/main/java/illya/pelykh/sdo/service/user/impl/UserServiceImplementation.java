package illya.pelykh.sdo.service.user.impl;

import illya.pelykh.sdo.dao.UserDao;
import illya.pelykh.sdo.model.UserModel;
import illya.pelykh.sdo.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImplementation implements UserService {

    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDao.findUserByUserName(username);
    }

    @Override
    public UserDetails loadUserById(Long id) {
        return userDao.findUserById(id);
    }

    @Override
    public void saveUser(final UserModel userModel) {
        userDao.save(userModel);
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}