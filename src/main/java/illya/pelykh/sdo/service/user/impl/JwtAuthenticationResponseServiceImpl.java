package illya.pelykh.sdo.service.user.impl;

import illya.pelykh.sdo.dao.JwtAuthenticationResponseDao;
import illya.pelykh.sdo.model.JwtAuthenticationResponseModel;
import illya.pelykh.sdo.service.user.JwtAuthenticationResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class JwtAuthenticationResponseServiceImpl implements JwtAuthenticationResponseService {

    private JwtAuthenticationResponseDao dao;

    @Override
    public JwtAuthenticationResponseModel findByToken(String token) {
        return dao.findJwtAuthenticationResponseModelByAccessToken(token);
    }

    @Override
    public void saveToken(JwtAuthenticationResponseModel jwtAuthenticationResponseModel) {
        dao.save(jwtAuthenticationResponseModel);
    }

    @Override
    public void deleteToken(String accessToken) {
        dao.deleteByAccessToken(accessToken);
    }

    @Autowired
    public void setDao(JwtAuthenticationResponseDao dao) {
        this.dao = dao;
    }
}