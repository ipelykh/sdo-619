package illya.pelykh.sdo.service.user;

import illya.pelykh.sdo.model.UserModel;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserDetails loadUserByUsername(String username);

    UserDetails loadUserById(Long id);

    void saveUser(UserModel userModel);
}
