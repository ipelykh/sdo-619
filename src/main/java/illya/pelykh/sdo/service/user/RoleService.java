package illya.pelykh.sdo.service.user;

import illya.pelykh.sdo.model.RoleModel;

public interface RoleService {

    RoleModel findByRole(String role);
}
