package illya.pelykh.sdo.service.user.impl;

import illya.pelykh.sdo.dao.RoleDao;
import illya.pelykh.sdo.model.RoleModel;
import illya.pelykh.sdo.service.user.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImplementation implements RoleService {

    private RoleDao roleDao;

    @Override
    public RoleModel findByRole(String role) {
        return roleDao.findByRole(role);
    }

    @Autowired
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }
}
