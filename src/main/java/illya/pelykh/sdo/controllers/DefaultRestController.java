package illya.pelykh.sdo.controllers;

import illya.pelykh.sdo.dto.ArticleDto;
import illya.pelykh.sdo.jwt.JwtTokenProvider;
import illya.pelykh.sdo.model.UserModel;
import illya.pelykh.sdo.service.executor.CallableTaskExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/news")
public class DefaultRestController {

    private final static Logger LOG = LogManager.getLogger(DefaultRestController.class);

    private final static String API_SOURCE = "https://newsapi.org/v2/everything";
    private final static String QUESTION_MARK = "?";
    private final static String AMPERSAND = "&";
    private final static String KEY_WORD = "q=";
    /**
     * Dates has to be provided in format yyyy-mm-dd
     */
    private final static String DATE_FROM = "from=";
    private final static String DATE_TO = "to=";
    private final static String API_KEY = "apiKey=bdd586af94a7424ab36800a9fd758eae";

    private CallableTaskExecutor executor;

    private JwtTokenProvider tokenProvider;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ArticleDto>> articles(@RequestParam final String q1, @RequestParam final String q2,
                                                     @RequestParam final String q3, @RequestParam final String from,
                                                     @RequestParam final String to) {
        UserModel currentUser = (UserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (tokenProvider.validateToken(currentUser.getTokenModel().getAccessToken())) {
            String urlFirst = API_SOURCE + QUESTION_MARK + KEY_WORD + q1 + AMPERSAND + DATE_FROM + from + AMPERSAND + DATE_TO + to + AMPERSAND + API_KEY;
            String urlSecond = API_SOURCE + QUESTION_MARK + KEY_WORD + q2 + AMPERSAND + DATE_FROM + from + AMPERSAND + DATE_TO + to + AMPERSAND + API_KEY;
            String urlThird = API_SOURCE + QUESTION_MARK + KEY_WORD + q3 + AMPERSAND + DATE_FROM + from + AMPERSAND + DATE_TO + to + AMPERSAND + API_KEY;

            List<String> urls = Arrays.asList(urlFirst, urlSecond, urlThird);

            try {
                return ResponseEntity.ok(executor.execute(urls));
            } catch (IllegalStateException | InterruptedException ex) {
                LOG.error(ex.getMessage());
            }
        }
        return ResponseEntity.badRequest().body(Collections.EMPTY_LIST);
    }

    @Autowired
    public void setExecutor(CallableTaskExecutor executor) {
        this.executor = executor;
    }

    @Autowired
    public void setTokenProvider(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }
}