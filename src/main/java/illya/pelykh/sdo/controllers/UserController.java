package illya.pelykh.sdo.controllers;

import illya.pelykh.sdo.configuration.CustomWebSecurityConfigurerAdapter;
import illya.pelykh.sdo.jwt.JwtTokenProvider;
import illya.pelykh.sdo.model.JwtAuthenticationResponseModel;
import illya.pelykh.sdo.model.UserModel;
import illya.pelykh.sdo.service.user.JwtAuthenticationResponseService;
import illya.pelykh.sdo.service.user.RoleService;
import illya.pelykh.sdo.service.user.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Objects;

@Controller
@RequestMapping("/user")
public class UserController {

    private final static Logger LOG = LogManager.getLogger(UserController.class);

    private UserService userService;

    private CustomWebSecurityConfigurerAdapter adapter;

    private RoleService roleService;

    private AuthenticationManager authenticationManager;

    private JwtTokenProvider tokenProvider;

    private JwtAuthenticationResponseService jwtService;

    @GetMapping("/do_logout")
    public void logout(final HttpServletRequest request, final HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            LOG.info("User " + auth.getName() + " logged out");
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

    @PostMapping("/do_login")
    public ResponseEntity<?> authenticateUser(@RequestParam final String userName, @RequestParam final String password) {

        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(userName, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.provideToken(authentication);
            LOG.info("User " + userName + " logged in");
            UserModel currentUser = (UserModel) userService.loadUserByUsername(userName);
            JwtAuthenticationResponseModel jwtToken = new JwtAuthenticationResponseModel(jwt, "Bearer");
            jwtService.saveToken(jwtToken);
            if (Objects.nonNull(currentUser.getTokenModel())) {
                jwtService.deleteToken(currentUser.getTokenModel().getAccessToken());
            }
            currentUser.setTokenModel(jwtService.findByToken(jwt));
            userService.saveUser(currentUser);
            return ResponseEntity.ok(jwtToken);
        } catch (AuthenticationException ex) {
            LOG.error(ex.getMessage());
        }
        return ResponseEntity.badRequest().body("Bad Credentials");
    }

    @PostMapping("/register")
    public void register(@RequestParam final String userName, @RequestParam final String password) {
        UserModel user = new UserModel();
        user.setUserName(userName);
        user.setPassword(adapter.passwordEncoder().encode(password));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        user.setRoles(Arrays.asList(roleService.findByRole("ROLE_USER")));
        try {
            userService.saveUser(user);
            LOG.info("User with name " + user.getUsername() + " successfully saved");
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setAdapter(CustomWebSecurityConfigurerAdapter adapter) {
        this.adapter = adapter;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setTokenProvider(JwtTokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Autowired
    public void setJwtService(JwtAuthenticationResponseService jwtService) {
        this.jwtService = jwtService;
    }
}