package illya.pelykh.sdo.dto;

import illya.pelykh.sdo.dto.impl.NewsApiResponseDto;

import java.io.Serializable;

public abstract class ResponseDto<T extends ResponseDto> implements Serializable {

    public abstract T getInstance(String status);
}