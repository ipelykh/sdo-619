package illya.pelykh.sdo.dto.impl;

import illya.pelykh.sdo.dto.ArticleDto;
import illya.pelykh.sdo.dto.ResponseDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NewsApiResponseDto extends ResponseDto {

    private String status;

    private int totalResults;

    private List<ArticleDto> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public List<ArticleDto> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleDto> articles) {
        this.articles = articles;
    }

    public NewsApiResponseDto() {
    }

    public NewsApiResponseDto(String status) {
        this.status = status;
    }

    @Override
    public NewsApiResponseDto getInstance(String status) {
        return new NewsApiResponseDto(status);
    }
}
