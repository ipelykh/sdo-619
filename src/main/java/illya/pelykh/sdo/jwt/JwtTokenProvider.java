package illya.pelykh.sdo.jwt;

import illya.pelykh.sdo.model.UserModel;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.TextCodec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {

    private final static Logger LOG = LogManager.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private Long jwtExpirationInMs;

    public String provideToken(final Authentication authentication) {
        UserModel user = (UserModel) authentication.getPrincipal();
        Date now = new Date();
        Date expiryTime = new Date(now.getTime() + jwtExpirationInMs);
        return Jwts.builder()
//                .setHeaderParam("typ", "JWT")
                .setSubject(user.getId().toString())
//                .setIssuedAt(new Date())
//                .setExpiration(expiryTime)
                .signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.decode(jwtSecret))
                .compact();
    }

    public Long getUserIdFromJwt(final String token) {

        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return Long.parseLong(claims.getSubject());
    }

    public Boolean validateToken(final String token) {
        try {
            Jwts.parser().setSigningKey(TextCodec.BASE64.decode(jwtSecret)).parseClaimsJwt(token);
            return true;
        } catch (SignatureException ex) {
            LOG.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            LOG.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            LOG.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            LOG.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            LOG.error("JWT claims string is empty.");
        }
        return false;
    }
}