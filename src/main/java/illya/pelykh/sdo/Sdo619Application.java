package illya.pelykh.sdo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sdo619Application {

    public static void main(String[] args) {
        SpringApplication.run(Sdo619Application.class, args);
    }
}
