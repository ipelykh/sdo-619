package illya.pelykh.sdo.dao;

import illya.pelykh.sdo.model.JwtAuthenticationResponseModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JwtAuthenticationResponseDao extends JpaRepository<JwtAuthenticationResponseModel, Long> {

    JwtAuthenticationResponseModel findJwtAuthenticationResponseModelByAccessToken(String accessToken);

    void deleteByAccessToken(String accessToken);
}