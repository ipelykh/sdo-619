package illya.pelykh.sdo.dao;

import illya.pelykh.sdo.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<RoleModel, Long> {
    RoleModel findByRole(String role);
}
