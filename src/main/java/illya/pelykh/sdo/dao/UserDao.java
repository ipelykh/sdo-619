package illya.pelykh.sdo.dao;

import illya.pelykh.sdo.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserDao extends JpaRepository<UserModel, Long> {

    UserDetails findUserByUserName(String name);

    UserDetails findUserById(Long id);
}